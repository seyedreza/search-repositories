const Get = (search, page = 0) => (
  new Promise((resolve, reject) => {
    fetch(`https://api.github.com/search/repositories?q=${search}&since=${page}&sort=stars&order=desc`)
      .then(res => res.json())
      .then(res => resolve(res))
      .catch(err => reject(err))
  })
)

export {
  Get
}