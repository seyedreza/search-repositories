import React, { PureComponent } from 'react';
// import Input from '../../component/Input';
// import { Get } from '../../configs/Rest';
// import debounce from 'lodash/debounce';
// import get from 'lodash/get';
// import { debounceCheckSearch } from '../../configs/globals';
// import Pagination from '../../component/Pagination';

import styles from './App.module.css';
//https://medium.com/@agoiabeladeyemi/pagination-in-reactjs-36f4a6a6eb43

export default class Repositories extends PureComponent {

  state = {
    users: null,
    total: null,
    per_page: null,
    current_page: null
  }

  componentDidMount() {
    this.makeHttpRequestWithPage(1);
  }

  makeHttpRequestWithPage = async pageNumber => {
    const response = await fetch(`https://reqres.in/api/users?page=${pageNumber}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    });

    const data = await response.json();

    this.setState({
      users: data.data,
      total: data.total,
      per_page: data.per_page,
      current_page: data.page
    });
  }

  // constructor (props) {
  //   super(props);
  //   this.state = {
  //     searchText: '',
  //     isLoading: false,
  //     errorMessage: '',
  //     list: [],
  //     isTyping: false,
  //     page: 0,
  //     totalCount: 0
  //   };
  //   this.updateSearch = debounce(this.updateSearch, debounceCheckSearch);
  // }
  //
  //
  // componentDidMount() {
  //
  // }

  // handleChangeSearch = value =>
  //   this.setState({
  //     searchText: value,
  //     isTyping: true,
  //     page: 0
  //   }, () => this.updateSearch());
  //
  // updateSearch = () => {
  //   if (this.state.searchText.length) {
  //     this.setState({ isLoading: true, isTyping: false }, () => {
  //       Get(this.state.searchText, this.state.page).then(res => {
  //         console.log('res', res)
  //         this.setState({
  //           isLoading: false,
  //           list: get(res, 'items', []),
  //           totalCount: res.total_count
  //         });
  //       }).catch(err => {
  //         console.log('err', err)
  //         this.setState({
  //           isLoading: false,
  //           totalCount: 0
  //         });
  //       });
  //     });
  //   } else {
  //
  //   }
  // };
  //
  // renderResult = () => {
  //
  // };

  render () {
    let users, renderPageNumbers;

    if (this.state.users !== null) {
      users = this.state.users.map(user => (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.first_name}</td>
          <td>{user.last_name}</td>
        </tr>
      ));
    }

    const pageNumbers = [];
    if (this.state.total !== null) {
      for (let i = 1; i <= Math.ceil(this.state.total / this.state.per_page); i++) {
        pageNumbers.push(i);
      }


      renderPageNumbers = pageNumbers.map(number => {
        let classes = this.state.current_page === number ? styles.active : '';

        if (number === 1 || number === this.state.total || (number >= this.state.current_page - 2 && number <= this.state.current_page + 2)) {
          return (
            <span key={number} className={classes} onClick={() => this.makeHttpRequestWithPage(number)}>{number}</span>
          );
        }
      });
    }

    return (


      <div className={styles.app}>

        <table className={styles.table}>
          <thead>
          <tr>
            <th>S/N</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
          </thead>
          <tbody>
          {users}
          </tbody>
        </table>


        <div className={styles.pagination}>
          <span onClick={() => this.makeHttpRequestWithPage(1)}>&laquo;</span>
          {renderPageNumbers}
          <span onClick={() => this.makeHttpRequestWithPage(1)}>&raquo;</span>
        </div>

      </div>
    );


    // return (
    //   <div>
    //     <div>
    //       <Input
    //         value={this.state.searchText}
    //         onChange={this.handleChangeSearch}
    //         placeholder='search'
    //       />
    //     </div>
    //     {this.renderResult()}
    //     <div>
    //       {
    //         this.state.list.map(item => {
    //           return (
    //             <div key={item.id}>{item.id}</div>
    //           )
    //         })
    //       }
    //       table
    //     </div>
    //     <div>
    //       <Pagination
    //         totalCount={this.state.totalCount}
    //       />
    //     </div>
    //   </div>
    // )
  }
}
