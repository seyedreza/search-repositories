import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DivContainer = styled.div`
  input.search-input {
    height: 20px;
    width: 30%;
  }
`;

const Input = (props) => {

  const handleChangeInput = event => props.onChange && props.onChange(event.target.value);

  return (
    <DivContainer>
      <input
        className='search-input'
        type='text'
        value={props.value}
        onChange={handleChangeInput}
        placeholder={props.placeholder}
      />
    </DivContainer>
  )
}

Input.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string
};

Input.defaultProps = {
  value: '',
  placeholder: ''
};

export default memo(Input);