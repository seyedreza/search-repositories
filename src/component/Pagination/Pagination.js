import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Pagination extends PureComponent {

  static propTypes = {
    totalCount: PropTypes.number.isRequired
  };

  state = {
    total: this.props.totalCount,
    perPage: null,
    currentPage: 1
  };

  computePage = number => {};

  renderPageNumbers = () => {
    const pageNumbers = [];
    if (this.state.total !== 0) {
      for (let i = 1; i <= Math.ceil(this.state.total / this.state.perPage); i++) {
        pageNumbers.push(i);
      }


      return pageNumbers.map(number => {
        let classes = this.state.currentPage === number ? 'active' : '';
        return (
          <span key={number} className={classes} onClick={() => this.computePage(number)}>{number}</span>
        );
      });
    }
  }

  render () {
    return (
      <div >
        <span onClick={() => this.computePage(1)}>&laquo;</span>
        {this.renderPageNumbers()}
        <span onClick={() => this.computePage(1)}>&raquo;</span>
      </div>
    )
  }
}